#!/usr/bin/env bash

set -e

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$(realpath $BIN_DIR'/../')/";

cd $PROJECT_DIR

if [ "${APP_ENV}" == "prod" ]; then
  composer install --no-dev --classmap-authoritative
  composer dump-autoload --optimize
else
  composer install
fi
