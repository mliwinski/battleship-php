#!/bin/bash

set -e

BINDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
IMAGE_VERSION=''

USAGE_STRING=$"Budowanie obrazu z podanego katalogu

Użycie: \t build.dev [opcje] katalog_z_dockerfile [parametry dla docker build]

Opcje:

  -t <nr_wersji> \t taguje obraz na podaną wersję (domyślnie: '${IMAGE_VERSION}')
  -V \t\t\t taguje obraz na wersję podaną w pliku Dockerfile
  -p pushuje obraz to naszego rejestru
  -d context budowania (domyślnie katalog ./)

  -h \t\t\t wyświetla ten opis
 "

if [ -z "$1" ]; then
  echo -e "$USAGE_STRING"
fi
PUSH_IMAGE=0
BUILD_CONTEXT=

for param in "$@"
do
    case "$param" in
      -h)
        echo -e "$USAGE_STRING"
        ;;
      -t)
        shift 1
        IMAGE_VERSION=":$1"
        shift 1
        ;;
      -V)
        shift 1
        IMAGE_VERSION=
        ;;
      -p)
        shift 1
        PUSH_IMAGE=1
        ;;
      -d)
        shift 1
        BUILD_CONTEXT=$1
        shift 1
        ;;
    esac
done

if [ -z "$1" ]; then
    echo "Brak argumentu z katalogiem do budowania"
    exit 1
fi;

DOCKERFILE_DIR=$1
shift 1

# ustawiam domyślny context jeżeli nie podano
if [ -z "${BUILD_CONTEXT}" ]; then
    BUILD_CONTEXT="./"
fi;

BUILD_PARAMS=$@

# parsujemy Dockerfile w poszukiwaniu rejestru obrazów oraz jego wersji
IMAGE_REGISTRY=`cat ${DOCKERFILE_DIR}/Dockerfile | grep " IMAGE_REGISTRY=" | sed -e 's/.*IMAGE_REGISTRY="//g;s/" \\\\//g'`

echo "Budowanie obrazu '"${IMAGE_REGISTRY}${IMAGE_VERSION}"'";
docker build -t ${IMAGE_REGISTRY}${IMAGE_VERSION} ${BUILD_PARAMS} -f ${DOCKERFILE_DIR}/Dockerfile ${BUILD_CONTEXT}

if [ ${PUSH_IMAGE} = 1 ]; then
    docker push ${IMAGE_REGISTRY}${IMAGE_VERSION}
fi
