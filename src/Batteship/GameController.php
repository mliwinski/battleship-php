<?php

namespace Battleship;

use InvalidArgumentException;

class GameController
{

    public static function checkIsHit(Board $fleet, Position $shot)
    {
        foreach ($fleet->ships as $ship) {
            /** @var Position $position */
            foreach ($ship->getPositions() as $position) {
                if ((string)$position == (string)$shot) {
                    $position->shoot();
                    $shipHealth = $ship->getHealth();
                    $ship->setHealth(max(0, $shipHealth - 1));
                    return true;
                }
            }
        }

        $fleet->addMiss($shot);
        return false;
    }

    public static function checkShipSunk(Board $fleet)
    {
        if ($fleet == null) {
            throw new InvalidArgumentException("ships is null");
        }
        foreach ($fleet->ships as $ship) {
            $shipHealth = $ship->getHealth();
            if ($shipHealth === 0) {
                return true;
            }
        }


        return false;
    }

    public static function initializeShips()
    {
        return Array(
            new Ship("Aircraft Carrier", 5, Color::BLUE),
            new Ship("Battleship", 4, Color::RED),
            new Ship("Submarine", 3, Color::YELLOW),
            new Ship("Destroyer", 3, Color::GREEN),
            new Ship("Patrol Boat", 2, Color::MAGENTA),
        );
    }

    public static function isShipValid($ship)
    {
        return count($ship->getPositions()) == $ship->getSize();
    }


    public static $shotRandomPositions = [];
    public static function getRandomPosition()
    {
        $position = static::generateRandomPosition();

        $limit = 65;

        while (isset(static::$shotRandomPositions[(string)$position]) && $limit-->0) {
            $position = static::generateRandomPosition();
        }

        static::$shotRandomPositions[(string)$position] = $position;

        return $position;
    }

    private static function generateRandomPosition(): Position
    {
        $rows = 8;
        $lines = 8;

        $letters = Letter::cases();

        return new Position(
            $letters[random_int(0, $lines - 1)],
            new Number(random_int(1, $rows))
        );
    }
}
