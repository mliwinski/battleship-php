<?php

declare(strict_types=1);

namespace Battleship;

class Board
{
    public const MARKER_SHIP = ' S ';
    public const MARKER_SHIP_HIT = ' X ';
    public const MARKER_MISS = ' - ';
    public const MARKER_WATER = '   ';

    /** @var Ship[] */
    public array $ships = [];
    /** @var Position[] */
    public array $misses = [];

    /**
     * @param Ship[] $ships
     */
    public function __construct(array $ships)
    {
        foreach ($ships as $ship) {
            $this->addShip($ship);
        }
    }

    public function addShip(Ship $ship)
    {
        $this->ships[] = $ship;
    }

    public function isPositionFree(Position $position)
    {
        foreach ($this->ships as $ship) {
            foreach ($ship->getPositions() as $taken) {
                if ((string)$position === (string)$taken) {
                    return false;
                }
            }
        }

        return true;
    }

    public function addMiss(Position $position)
    {
        $position->shoot();
        $this->misses[(string)$position] = $position;
    }

    public function print(\Console $console, bool $onlyShot = true)
    {
        $console->setColor(\Battleship\Color::YELLOW);
        $console->println('__A__B__C__D__E__F__G__H__');

        foreach (Number::$range as $number) {
            $console->setColor(\Battleship\Color::YELLOW);
            $console->print($number);

            foreach (Letter::cases() as $case) {
                $current = new Position($case, new Number($number));

                if ($this->printShipMarker($current, $console, $onlyShot)) {
                    continue;
                }

                if ($this->printMissMarker($current, $console)) {
                    continue;
                }

                $console->resetColor();
                $console->print(static::MARKER_WATER);
            }

            $console->setColor(\Battleship\Color::YELLOW);
            $console->println($number);
        }

        $console->setColor(\Battleship\Color::YELLOW);
        $console->println('__A__B__C__D__E__F__G__H__');
        $console->resetColor();
    }

    private function printShipMarker(Position $current, \Console $console, bool $onlyShot = true)
    {
        /** @var Ship $ship */
        foreach ($this->ships as $ship) {
            foreach ($ship->getPositions() as $position) {
                if ((string)$position === (string)$current) {
                    if ($position->shoot && $onlyShot) {
                        $console->setColor($ship->getColor());
                        $console->setColor(Color::WHITE_BG);
                        $console->print(static::MARKER_SHIP_HIT);
                        $console->resetColor();
                        return true;
                    }

                    if (!$onlyShot) {
                        $console->setColor($ship->getColor());
                        $console->setColor(Color::WHITE_BG);
                        $console->print($position->shoot ? static::MARKER_SHIP_HIT : static::MARKER_SHIP);
                        $console->resetColor();
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private function printMissMarker(Position $current, \Console $console)
    {
        foreach ($this->misses as $position) {
            if ((string)$position === (string)$current) {
                $console->setColor(Color::BLUE);
                $console->setColor(Color::LIGHT_BLUE_BG);
                $console->print(static::MARKER_MISS);
                $console->resetColor();
                return true;
            }
        }

        return false;
    }
}
