<?php

namespace Battleship;

class Color
{
    const RED = "\e[31m";
    const RED_BG = "\e[41m";
    const GREEN = "\e[32m";
    const GREEN_BG = "\e[42m";
    const YELLOW = "\e[33m";
    const YELLOW_BG = "\e[43m";
    const BLUE = "\e[34m";
    const BLUE_BG = "\e[44m";
    const MAGENTA = "\e[35m";
    const MAGENTA_BG = "\e[45m";
    const CYAN = "\e[36m";
    const CYAN_BG = "\e[46m";
    const LIGHT_GRAY = "\e[37m";
    const LIGHT_GRAY_BG = "\e[47m";
    const GRAY = "\e[90m";
    const GRAY_BG = "\e[100m";
    const LIGHT_RED = "\e[91m";
    const LIGHT_RED_BG = "\e[101m";
    const LIGHT_GREEN = "\e[92m";
    const LIGHT_GREEN_BG = "\e[102m";
    const LIGHT_YELLOW = "\e[93m";
    const LIGHT_YELLOW_BG = "\e[103m";
    const LIGHT_BLUE = "\e[94m";
    const LIGHT_BLUE_BG = "\e[104m";
    const LIGHT_MAGENTA = "\e[95m";
    const LIGHT_MAGENTA_BG = "\e[105m";
    const LIGHT_CYAN = "\e[96m";
    const LIGHT_CYAN_BG = "\e[106m";
    const WHITE = "\e[97m";
    const WHITE_BG = "\e[107m";

    const RESET = "\e[0m";
}
