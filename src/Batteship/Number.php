<?php

namespace Battleship;

use InvalidArgumentException;

class Number
{
    public static array $range = [1, 2, 3, 4, 5, 6, 7, 8];

    public function __construct(public int $value)
    {
        if (!in_array($this->value, static::$range, false)) {
            throw new \Exception('Number outside of range.');
        }
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }
}
