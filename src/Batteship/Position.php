<?php

namespace Battleship;

use \InvalidArgumentException;

class Position
{
    public bool $shoot = false;

    public function shoot()
    {
        $this->shoot = true;
    }


    public static function _($code)
    {
        if (strlen($code) > 2) {
            throw new \Exception("Invalid position. Use format <A-H><1-8>, for instance: B3.");
        }
        return new static(
            Letter::from(strtoupper(substr($code, 0, 1))),
            new Number((int)(substr($code, 1, 1)))
        );
    }

    /**
     * Position constructor.
     * @param string $column
     * @param string $row
     */
    public function __construct(protected Letter $column, protected Number $row)
    {
    }

    public function getColumn()
    {
        return $this->column;
    }

    public function getRow()
    {
        return $this->row;
    }

    public function __toString()
    {
        return sprintf("%s%s", $this->column->value, $this->row);
    }
}
