<?php

namespace Battleship;

class Ship
{

    private $name;
    private $size;
    private $color;
    /** @var Position[] */
    private $positions = array();
    private $health;

    public function __construct($name, $size, $color = null)
    {
        $this->name = $name;
        $this->size = $size;
        $this->health = $size;
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @return integer
     */
    public function getHealth()
    {
        return $this->health;
    }

    public function addPosition(Position $position)
    {
        array_push($this->positions, $position);
    }

    public function &getPositions()
    {
        return $this->positions;
    }

    public function isPositionAdjucent(Position $new)
    {
        if (empty($this->positions)) {
            return true;
        }

        foreach ($this->positions as $position) {
            if ($this->areTwoPositionsAdjucent($position, $new)) {
                return true;
            }
        }

        return false;
    }

    public function isPositionInLine(Position $new)
    {
        if (empty($this->positions)) {
            return true;
        }

        if (count($this->positions) === 1) {
            return $this->areTwoPositionsAdjucent($this->positions[0], $new);
        }

        $cols = $rows = [];

        foreach ($this->positions as $position) {
            $cols[$position->getColumn()->value] = $position;
            $rows[$position->getRow()->value] = $position;
        }

        if (count($cols)>1) {
            ksort($cols);
            $letters = Letter::cases();
            $firstCol = reset($cols);
            $col = array_search($firstCol->getColumn(), $letters);
            $colLeft = $col-1;

            if (
                $colLeft >= 0 &&
                $letters[$colLeft] === $new->getColumn() &&
                $firstCol->getRow()->value === $new->getRow()->value
            ) {
                return true;
            }

            $lastCol = end($cols);
            $col = array_search($lastCol->getColumn(), $letters);
            $colRight = $col+1;

            if (
                $colRight< 9 &&
                $letters[$colRight] === $new->getColumn() &&
                $lastCol->getRow()->value === $new->getRow()->value
            ) {
                return true;
            }
        }

        if (count($rows)>1) {
            ksort($rows);
            $firstRow = reset($rows);
            $rowUp = $firstRow->getRow()->value-1;

            if (
                $rowUp > 0 &&
                $firstRow->getColumn() === $new->getColumn() &&
                $rowUp === $new->getRow()->value
            ) {
                return true;
            }

            $lastRow = end($rows);
            $rowDown = $lastRow->getRow()->value+1;

            if (
                $rowDown < 9 &&
                $lastRow->getColumn() === $new->getColumn() &&
                $rowDown === $new->getRow()->value
            ) {
                return true;
            }
        }

        return false;

    }

    private function areTwoPositionsAdjucent(Position $existing, Position $new)
    {
        $letters = Letter::cases();
        $col = array_search($existing->getColumn(), $letters);
        $row = $existing->getRow();

        $colLeft = $col-1;
        $colRight = $col+1;

        $rowUp = $row->value-1;
        $rowDown = $row->value+1;

        if (
            $colLeft >= 0 &&
            $letters[$colLeft] === $new->getColumn() &&
            $row->value === $new->getRow()->value
        ) {
            return true;
        }

        if (
            $colRight < 8 &&
            $letters[$colRight] === $new->getColumn() &&
            $row->value === $new->getRow()->value
        ) {
            return true;
        }

        if (
            $rowUp > 0 &&
            $existing->getColumn() === $new->getColumn() &&
            $rowUp === $new->getRow()->value
        ) {
            return true;
        }

        if (
            $rowDown < 9 &&
            $existing->getColumn() === $new->getColumn() &&
            $rowDown === $new->getRow()->value
        ) {
            return true;
        }

        return false;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function setHealth($health)
    {
        $this->health = $health;
    }
}
