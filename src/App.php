<?php

use Battleship\GameController;
use Battleship\Position;
use Battleship\Letter;
use Battleship\Color;

class App
{
    private static \Battleship\Board $myFleet;
    private static \Battleship\Board$enemyFleet;
    private static bool $gameOver = false;
    /**
     * @var Console
     */
    private static Console $console;

    static function run()
    {
        self::$console = new Console();
        self::$console->println("                                     |__");
        self::$console->println("                                     |\\/");
        self::$console->println("                                     ---");
        self::$console->println("                                     / | [");
        self::$console->println("                              !      | |||");
        self::$console->println("                            _/|     _/|-++'");
        self::$console->println("                        +  +--|    |--|--|_ |-");
        self::$console->println("                     { /|__|  |/\\__|  |--- |||__/");
        self::$console->println("                    +---------------___[}-_===_.'____                 /\\");
        self::$console->println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        self::$console->println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        self::$console->println("|                        Welcome to Battleship                         BB-61/");
        self::$console->println(" \\_________________________________________________________________________|");
        self::$console->println();
        self::$console->resetColor();
        self::InitializeGame();
        self::StartGame();
    }

    public static function InitializeEnemyFleet()
    {
        self::$enemyFleet = new \Battleship\Board(GameController::initializeShips());

        foreach (static::getRandomPlacement() as $index=>$ship) {
            foreach ($ship as $position) {
                array_push(self::$enemyFleet->ships[$index]->getPositions(), Position::_($position));
            }
        }
    }

    public static function getRandomPlacement()
    {
        $variants = [
            [
                ['b4', 'b5', 'b6', 'b7', 'b8'],
                ['e5', 'e6', 'e7', 'e8'],
                ['a3', 'b3', 'c3'],
                ['f8', 'g8', 'h8'],
                ['c5', 'c6']
            ],
            [
                ['a1', 'a2', 'a3', 'a4', 'a5'],
                ['d1', 'd2', 'd3', 'd4'],
                ['g5', 'g6', 'g7'],
                ['c6', 'c7', 'c8'],
                ['h5', 'h6']
            ],
            [
                ['d1', 'e1', 'f1', 'g1', 'h1'],
                ['a7', 'b7', 'c7', 'd7'],
                ['c3', 'c4', 'c5'],
                ['d3', 'e3', 'f3'],
                ['a1', 'a2']
            ],
            [
                ['b1', 'b2', 'b3', 'b4', 'b5'],
                ['a7', 'b7', 'c7', 'd7'],
                ['f3', 'g3', 'h3'],
                ['f8', 'g8', 'h8'],
                ['h1', 'h2']
            ],
            [
                ['a1', 'a2', 'a3', 'a4', 'a5'],
                ['c7', 'd7', 'e7', 'f7'],
                ['f4', 'f5', 'f6'],
                ['f8', 'g8', 'h8'],
                ['g1', 'h1']
            ]
        ];

        return $variants[array_rand($variants, 1)];
    }

    public static function InitializeMyFleet()
    {
        static::$myFleet = new \Battleship\Board(GameController::initializeShips());

        self::$console->println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        foreach (self::$myFleet->ships as $ship) {

            self::$console->println();
            printf("Please enter the positions for the %s (size: %s)", $ship->getName(), $ship->getSize());

            for ($i = 1; $i <= $ship->getSize(); $i++) {
                $entering = true;
                while ($entering) {
                    static::$console->resetColor();
                    printf("\nEnter position %s of %s (i.e A3):", $i, $ship->getSize());
                    $input = readline("");

                    try {
                        $position = self::parsePosition($input);

                        if (!static::$myFleet->isPositionFree($position)) {
                            static::$console->setColor(Color::RED);
                            static::$console->println("Invalid position `{$position}`. This point is already part of a ship.");
                            continue;
                        }

                        if (!$ship->isPositionAdjucent($position)) {
                            static::$console->setColor(Color::RED);
                            static::$console->println("Invalid position `{$position}`: ship cannot have gaps.");
                            continue;
                        }

                        if (!$ship->isPositionInLine($position)) {
                            static::$console->setColor(Color::RED);
                            static::$console->println("Invalid position `{$position}`. Ship must be vertical or horizontal.");
                            continue;
                        }

                        $ship->addPosition($position);
                        $entering = false;
                    } catch (Exception|ValueError $e) {
                        static::$console->setColor(Color::RED);
                        self::$console->println("Invalid position `{$input}`. Use format <A-H><1-8>, for instance: B3.");
                    }
                }
            }
        }
    }

    public static function beep()
    {
        echo "\007";
    }

    public static function clear()
    {
        echo "\033c";
    }

    public static function InitializeGame()
    {
        self::InitializeMyFleet();
        self::InitializeEnemyFleet();
    }

    public static function StartGame()
    {
        static::clear();

        while (true) {

            self::$console->println("");
            self::$console->println("Player, it's your turn");
            self::$console->println("");
            self::$console->println('Enemy fleet:');
            static::$enemyFleet->print(static::$console);
            self::$console->println("");
            self::$console->println('My fleet:');
            static::$myFleet->print(static::$console, false);
            self::$console->println();
            $entering = true;
            while ($entering) {
                self::$console->println("Enter coordinates for your shot :");
                $readline = readline("");

                try {
                    $position = self::parsePosition($readline);
                    $entering = false;
                } catch (Exception|ValueError $e) {
                    static::$console->setColor(Color::RED);
                    self::$console->println("Invalid position `{$readline}`. Use format <A-H><1-8>, for instance: B3.");
                    static::$console->resetColor();
                }
            }

            static::clear();

            $isHit = GameController::checkIsHit(self::$enemyFleet, $position);

            if ($isHit) {
                self::$gameOver = self::checkGameOver(self::$enemyFleet);
                if (self::$gameOver) {
                    static::$console->println('You won! Congratulations!');
                    exit(0);
                }

                if (GameController::checkShipSunk(self::$enemyFleet)) {
                    static::$console->println('Ship sunk!');
                }
                self::beep();
//                self::$console->println("                \\         .  ./");
//                self::$console->println("              \\      .:\" \";'.:..\" \"   /");
//                self::$console->println("                  (M^^.^~~:.'\" \").");
//                self::$console->println("            -   (/  .    . . \\ \\)  -");
//                self::$console->println("               ((| :. ~ ^  :. .|))");
//                self::$console->println("            -   (\\- |  \\ /  |  /)  -");
//                self::$console->println("                 -\\  \\     /  /-");
//                self::$console->println("                   \\  \\   /  /");
                self::$console->println("Yeah ! Nice hit !");

                if (GameController::checkShipSunk(self::$enemyFleet)) {
                    static::$console->println();
                    static::$console->println('Ship sunk!');
                }

                if (self::checkGameOver(self::$enemyFleet)) {
                    static::$console->println();
                    static::$console->println('You won! Congratulations!');
                    exit(0);
                }
            } else {
                self::$console->println("Miss");
            }

            $position = GameController::getRandomPosition();
            $isHit = GameController::checkIsHit(self::$myFleet, $position);
            self::$console->println();
            static::$console->println(
                sprintf(
                    "Computer shoot in %s%s and %s",
                    $position->getColumn()->value, $position->getRow(), $isHit ? "hit your ship !\n" : "miss"
                )
            );

            if ($isHit) {
                if (GameController::checkShipSunk(self::$myFleet)) {
                    self::$console->println();
                    static::$console->println('Ship sunk!');
                    self::$console->println();
                }
                if (self::checkGameOver(self::$myFleet)) {
                    static::$console->setColor(Color::BLUE_BG);
                    static::$console->setColor(Color::WHITE);
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println("                                  You die. All ships from Your fleet had been sunk.                                     ");
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                  * Press any key to attempt to continue.                                               ');
                    static::$console->println('                                  * Press CTRL+ALT+DEL to restart your computer. You will                               ');
                    static::$console->println('                                    lose any unsaved information in all applications.                                   ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');
                    static::$console->println('                                                                                                                        ');

                    static::$console->resetColor();

                    exit(0);
                }
                self::beep();
            }
        }
    }

    public static function parsePosition($input)
    {
        $number = substr($input, 1, 1);

        if(!is_numeric($number)) {
            throw new Exception("Invalid position. Use format <A-H><1-8>, for instance: B3.");
        }

        return Position::_($input);
    }


    public static function checkGameOver(\Battleship\Board $fleet)
    {
        if ($fleet == null) {
            throw new InvalidArgumentException("ships is null");
        }

        foreach ($fleet->ships as $ship) {
            $shipHealth = $ship->getHealth();
            if ($shipHealth !== 0) {
                return false;
            }
        }

        return true;
    }
}
