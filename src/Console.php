<?php

//use Battleship\Color;

class Console
{
    function resetColor()
    {
        echo(Battleship\Color::RESET);
    }

    function print($string = ''): void
    {
        echo "$string";
    }

    function setColor($color)
    {
        echo($color);
    }

    function println($line = "")
    {
        echo "$line\n";
    }
}
